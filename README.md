# Ackermann drive ros gazebo plugin

Плагин предоставляет возможность управления моделью транспортного средства типа Аккермана в Gazebo  под ROS 2 используя пользовательский тип сообщения AlphaDrive.msg, подробнее описан в пакете [alpha_emu_msgs](https://gitlab.com/sl_prak/alpha_emu_msgs). Сообщение данного типа предназначено для передачи данных в ROS с параметрами из [Alpha CAN protocol.](https://gitlab.com/starline/alpha/alpha_docs/-/blob/master/docs/can.md)

## Установка вне контейнера

В директории workspace/src выполните:

```bash
git clone https://gitlab.com/sl_prak/alpha_emu_gazebo_plugin.git
cd ..
rosdep install --from-paths src --ignore-src -r -y
colcon build
```

## О работе плагина

Данный плагин является изменённой версией плагина [gazebo_ros_ackermann_drive](https://github.com/ros-simulation/gazebo_ros_pkgs/blob/foxy/gazebo_plugins/src/gazebo_ros_ackermann_drive.cpp). В отличие от оригинальной версии принимает сообщение типа  [AlphaDrive.msg](https://gitlab.com/sl_prak/oscar_vehicle_simulator/-/blob/develop/simulator_ws/src/oscar_ros_gazebo_interfaces/msg/AlphaDrive.msg) вместо [geometry_msgs/Twist Message](http://docs.ros.org/en/api/geometry_msgs/html/msg/Twist.html).

- [Применение плагина к модели, комментарии к тегам xml](docs/plugin_usage.md)

- [xml шаблон](docs/gazebo_code.xml)

- [Больше информации о работе плагина](docs/about_plugin.md)
