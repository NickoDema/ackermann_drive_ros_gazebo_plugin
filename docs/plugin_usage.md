# Применение плагина к модели

Пример модели с использованием официального плагина доступен по [ссылке](https://github.com/ros-simulation/gazebo_ros_pkgs/blob/galactic/gazebo_plugins/worlds/gazebo_ros_ackermann_drive_demo.world).

Добавьте код из [файла](gazebo_code.xml) к вашей sdf или urdf модели.

При создании модели учтите [ограничения для колёс.](about_plugin.md#создание-объектов-и-загузка-моделиметод-gazeborosackermanndriveload)

## ROS

```<plugin name="gazebo_ros_ackermann_drive" filename="libackermann_drive_ros_gazebo_plugin.so">``` - важно правильно указать *.so файл.


- ```<namespace>``` - пространство имен, в котором будут публиковаться топики (пустой тег - использование глобального неймспейса)
- ```<remapping>cmd_vel:=cmd_actros</remapping>``` - переназначение имени топика, принимающего команды по управлению
- ```<remapping>odom:=odom_actros</remapping>``` - переназначение имени топика, в который будут публиковаться данные одометрии
- ```<remapping>distance:=distance_actros</remapping>``` - переназначение имени топика, в который будут публиковаться данные о пройденном пути
- ```<update_rate>``` - частота обновления [с].

## Входные данные

В соответствующие теги нужно передать джоинты соединения колес с непрерывным вращением. В случе urdf нужно использовать continuous joint, для sdf можно воспользоваться univerlal joint, как в [официальном примере](https://github.com/ros-simulation/gazebo_ros_pkgs/blob/galactic/gazebo_plugins/worlds/gazebo_ros_ackermann_drive_demo.world).

```xml
<front_left_joint>
<front_right_joint>
<rear_left_joint>
<rear_right_joint>
```

В следующие теги необходимо передать соединение поворота колес вокруг оси аппликат (поворот передних колес).  В случе urdf нужно использовать revolute joint, для sdf можно передать созданный ранее univerlal joint.

```xml
<left_steering_joint>
<right_steering_joint>
```

Следующий тег является опциональным, ему можно передать соединение модели и руля: в таком случе можно учесть ограничения вращения руля, добавить визуализацию вращения в симуляции и использовать это для других задач.

```xml
<steering_wheel_joint>
```

## Ограничения

Далее нужно передать соответственно:

- ```<max_steer>``` - максимальный угол поворота рулевого колеса при повороте в радианах (можно рассчитать конструктивно исходя из минимального радиуса поворота модели, [в данном примере](https://bstudy.net/644022/tehnika/kinematika_povorota) alpha_вн)

- ```<max_steering_angle>``` - максимальный угол поворота руля в радинах
- ```<max_speed>``` - максимальная скорость модели, лучше использовать те же значения, что и в ограничениях по скорости в джоинтах.

## ПИД регулятор

Коэффициенты для настройки [ПИД регулятора](https://ru.wikipedia.org/wiki/%D0%9F%D0%98%D0%94-%D1%80%D0%B5%D0%B3%D1%83%D0%BB%D1%8F%D1%82%D0%BE%D1%80) вращения рулевых колёс и скорости модели. Больше об этом можно прочитать в [соответствующем разделе файла о работе плагина.](about_plugin.md#пид-регулятор)

```xml
<left_steering_pid_gain>
<right_steering_pid_gain>
<linear_velocity_pid_gain>
```

Верхняя и нижняя границы регулирующих величин. При установке в поле "0 0" пределов регулирования не будет. В случае, если у модели заданы ограничения по скорости и углу поворота колёс, можно проигнорировать эти значения. 

```xml
<left_steering_i_range>
<right_steering_i_range>
<linear_velocity_i_range>
```

## Выходные данные плагина - создание топиков ROS

В данные теги нужно передать булевы значения: true при необходимости публикации соответствующей информации о модели в одноименные топики.

```xml
<publish_odom>
<publish_odom_tf>
<publish_wheel_tf>
<publish_distance>
```

Имя фрейма одометрии и базового фрейма модели:

```xml
<odometry_frame>
<robot_base_frame>
```